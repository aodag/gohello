package main

import "fmt"

func GetMessage(name string) string {
	return fmt.Sprintf("Hello, %s", name);
}

func Greeting() {
	fmt.Printf("%s\n", GetMessage("World"))
}
