package main

import "testing"

func TestGetMessage(t *testing.T) {
	const name = "aodag"
	const expect = "Hello, aodag"
	result := GetMessage(name);
	
	if result != expect {
		t.Errorf("%s != %s", result, expect)
	}
}
